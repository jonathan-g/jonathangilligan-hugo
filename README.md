# Personal Web Site

<https://jonathangilligan.org/>

[![Netlify Status](https://api.netlify.com/api/v1/badges/bbf72fd4-b6e0-4e81-baef-6babad221dce/deploy-status)](https://app.netlify.com/sites/jonathangilligan/deploys)

The site is hosted by Netlify and built with Hugo, using my [`hugo-finisterre`](https://github.com/jonathan-g/hugo-finisterre)
theme.
